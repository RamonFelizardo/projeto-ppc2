describe('Login', () => {
  describe('When visit login url', () => {
    const validEmail = 'email_50@gmail.com';
    const invalidEmail = 'my_email';
    const emailHasNoAccess = 'email_500@gmail.com';
    const validPassword = 'password50';
    const passwordHasNoAccess = 'password200';
    const invalidPassword = 'abc1';

    beforeEach(() => {
      cy.visit('/login');
    });

    it('contains h1 Login', () => {
      cy.contains('h1', 'Login');
    });

    describe('When the email is filled', () => {
      describe('When the email format is not valid', () => {
        beforeEach(() => {
          cy.get('.login_input_email').type(invalidEmail);
        });
        it('contains a message email format invalid', () => {
          cy.get('.login_h4_email').should('be.visible');
        });
      });

      describe('When the email format is valid', () => {
        beforeEach(() => {
          cy.get('.login_input_email').type(validEmail);
        });
        it('the message email format invalid is not visible', () => {
          cy.get('.login_h4_email').should('not.be.visible');
        });
      });
    });

    describe('When the password is filled', () => {
      describe('When the password size is not valid', () => {
        beforeEach(() => {
          cy.get('.login_input_password').type(invalidPassword);
        });
        it('contains a message password size invalid', () => {
          cy.get('.login_h4_password').should('be.visible');
        });
      });

      describe('When the password sizes is valid', () => {
        beforeEach(() => {
          cy.get('.login_input_password').type(validPassword);
        });
        it('the message password size invalid is not visible', () => {
          cy.get('.login_h4_password').should('not.be.visible');
        });
      });
    });

    describe('When fill email and password', () => {
      describe('When the email and the password is wrong', () => {
        beforeEach(() => {
          cy.get('.login_input_email').type(emailHasNoAccess);
          cy.get('.login_input_password').type(passwordHasNoAccess);
          cy.get('button').click();
        });
        it('contains a message wrong login', () => {
          cy.get('.login_wrong_login').should('be.visible');
        });
      });

      beforeEach(() => {
        cy.get('.login_input_email').type(validEmail);
        cy.get('.login_input_password').type(validPassword);
      });

      describe('When click button login', () => {
        it('should got to user form', () => {
          cy.url().should('eq', 'http://localhost:8080/login');
          cy.get('button').click();
          cy.url().should('eq', 'http://localhost:8080/userform');
        });
      });
    });
  });
});
