describe('User Form', () => {
  describe('When visit user form url', () => {
    const validEmail = 'email_1@gmail.com';
    const invalidEmail = 'email@';
    const underAge = '15';
    const aboveAge = '100';
    const letterAge = 'a18';
    const validAge = '20';
    const validSo = 'Windows';

    beforeEach(() => {
      cy.visit('/userform');
    });

    it('contains h1 userform', () => {
      cy.contains('h1', 'User Form');
    });

    describe('When the email is filled', () => {
      describe('When the email format is invalid', () => {
        beforeEach(() => {
          cy.get('.user_form_input_email').type(invalidEmail);
        });
        it('contains a message email format invalid', () => {
          cy.get('.user_form_h4_email_completed').should('be.visible');
        });
      });

      describe('When the email format is valid', () => {
        beforeEach(() => {
          cy.get('.user_form_input_email').type(validEmail);
        });
        it('message email format invalid disappe', () => {
          cy.get('.user_form_h4_email_completed').should('not.be.visible');
        });
      });
    });

    describe('When the age is filled', () => {
      describe('When the age is under 18', () => {
        beforeEach(() => {
          cy.get('.user_form_input_age').type(underAge);
        });
        it('contains a message the age it has to be a number from 18 to 99', () => {
          cy.get('.user_form_h4_age').should('be.visible');
        });
      });

      describe('When the age is above 99', () => {
        beforeEach(() => {
          cy.get('.user_form_input_age').type(aboveAge);
        });
        it('contains a message the age it has to be a number from 18 to 99', () => {
          cy.get('.user_form_h4_age').should('be.visible');
        });
      });

      describe('When the age contains a letter', () => {
        beforeEach(() => {
          cy.get('.user_form_input_age').type(letterAge);
        });
        it('contains a message the age it has to be a number from 18 to 99', () => {
          cy.get('.user_form_h4_age').should('be.visible');
        });
      });

      describe('When the age is valid', () => {
        beforeEach(() => {
          cy.get('.user_form_input_age').type(validAge);
        });
        it('message the age it has to be a number from 18 to 99 not be visible', () => {
          cy.get('.user_form_h4_age').should('not.be.visible');
        });
      });
    });

    describe('SO', () => {
      describe('When the SO is empty', () => {
        it('contains a message SO empty', () => {
          cy.get('.user_form_h4_so').should('be.visible');
        });
      });

      describe('When the SO is filled', () => {
        beforeEach(() => {
          cy.get('select').select(validSo).should('have.value', validSo);
        });
        it('the message SO empty not be visible', () => {
          cy.get('.user_form_h4_so').should('not.be.visible');
        });
      });
    });
  });
});
