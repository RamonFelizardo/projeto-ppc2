import { shallowMount } from '@vue/test-utils';
import Login from '@/components/Login.vue';

describe('Login', () => {
  it('is a valid component ', () => {
    const wrapper = shallowMount(Login);
    expect(wrapper.exists()).toBe(true);
  });

  it("should has h1 equals to 'Login'", () => {
    const wrapper = shallowMount(Login);
    expect(wrapper.find('h1').text()).toMatch('Login');
  });

  describe('When email is filled', () => {
    describe('When email format is invalid', () => {
      const wrapper = shallowMount(Login, {
        computed: { isEmailFilled: () => true },
      });
      it('should have a message email format invalid', () => {
        expect(wrapper.find('.login_h4_email').attributes('style')).not.toBe('display: none;');
      });
    });

    describe('When email format is valid', () => {
      const wrapper = shallowMount(Login, {
        computed: { isEmailFilled: () => false },
      });
      it('message email format invalid disapper', () => {
        expect(wrapper.find('.login_h4_email').attributes('style')).toBe('display: none;');
      });
    });
  });

  describe('When password is filled', () => {
    describe('When password size is invalid', () => {
      const wrapper = shallowMount(Login, {
        computed: { isPasswordFilled: () => true },
      });
      it('should have a message password size is invalid', () => {
        expect(wrapper.find('.login_h4_password').attributes('style')).not.toBe('display: none;');
      });
    });

    describe('When password is valid', () => {
      const wrapper = shallowMount(Login, {
        computed: { isPasswordFilled: () => false },
      });
      it('message password size invalid disapper', () => {
        expect(wrapper.find('.login_h4_password').attributes('style')).toBe('display: none;');
      });
    });
  });

  describe('When Login is completed', () => {
    describe('When has email and password', () => {
      describe('And email and password are valids', () => {
        it('should be true', () => {
          const wrapper = shallowMount(Login, {
            data: () => ({ email: 'email_50@gmail.com', password: 'password50' }),
          });
          expect(wrapper.vm.isLoginCompleted).toBe(true);
        });
      });
    });
  });

  describe('When Login is completed', () => {
    describe('When has email and password', () => {
      describe('And email and password has access', () => {
        it('should be true', () => {
          const wrapper = shallowMount(Login);
          expect(wrapper.vm.validateLogin('email_50@gmail.com', 'password50')).toBe(true);
        });
      });
      describe('And email and password does has access', () => {
        it('should be false', () => {
          const wrapper = shallowMount(Login);
          expect(wrapper.vm.validateLogin('email_500@gmail.com', 'password500')).toBe(false);
        });
      });
    });
  });
});
