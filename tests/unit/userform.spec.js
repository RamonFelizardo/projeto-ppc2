import { shallowMount } from '@vue/test-utils';
import UserForm from '@/components/UserForm.vue';

describe('UserForm', () => {
  it('is a valid component ', () => {
    const wrapper = shallowMount(UserForm);
    expect(wrapper.exists()).toBe(true);
  });

  it("should has h1 equals to 'UserForm'", () => {
    const wrapper = shallowMount(UserForm);
    expect(wrapper.find('h1').text()).toMatch('User Form');
  });

  describe('When email is filled', () => {
    describe('When email format is invalid', () => {
      const wrapper = shallowMount(UserForm, {
        computed: { isEmailFilled: () => true },
      });
      it('should have a message email format invalid', () => {
        expect(wrapper.find('.user_form_h4_email_completed').attributes('style')).not.toBe('display: none;');
      });
    });

    describe('When email format is valid', () => {
      const wrapper = shallowMount(UserForm, {
        computed: { isEmailFilled: () => false },
      });
      it('message email format invalid disapper', () => {
        expect(wrapper.find('.user_form_h4_email_completed').attributes('style')).toBe('display: none;');
      });
    });

    describe('When email its not the same used in login', () => {
      const wrapper = shallowMount(UserForm, {
        computed: { isEmailEquals: () => true },
      });
      it('should have message email its not the same filled in login', () => {
        expect(wrapper.find('.user_form_h4_email_equals').attributes('style')).not.toBe('display: none;');
      });
    });
  });

  describe('When age is filled', () => {
    describe('When age its not between 18 and 99', () => {
      const wrapper = shallowMount(UserForm, {
        computed: { isAgeRight: () => true },
      });
      it('should have message the age it has to be a number from 18 to 99', () => {
        expect(wrapper.find('.user_form_h4_age').attributes('style')).not.toBe('display: none;');
      });
    });

    describe('When age its not a number', () => {
      const wrapper = shallowMount(UserForm, {
        computed: { isAgeRight: () => true },
      });
      it('should have message the age should have message the age it has to be a number from 18 to 99', () => {
        expect(wrapper.find('.user_form_h4_age').attributes('style')).not.toBe('display: none;');
      });
    });
  });

  describe('When SO its not choosen', () => {
    const wrapper = shallowMount(UserForm, {
      computed: { isSoChosen: () => true },
    });
    it('should have message SO not choosen', () => {
      expect(wrapper.find('.user_form_h4_so').attributes('style')).not.toBe('display: none;');
    });
  });
});
